# Manually corrected Gender Recognition Datasets

Gender recognition datasets used in the literature are subject to include incorrect class labels. The repository contains manually corrected labels for Adience, Afad, CelebA, Genki-4K, Imdb, LFW, Academic Morph, UTKFace, VGGFace2, and Wiki datasets.

If you have used the manual annotations in a scientific publication, we would appreciate citations to the following paper:

Danisman, T., 2021, [Bagging ensemble for deep learning based gender recognition using test-time augmentation on large-scale datasets](https://online-journals.tubitak.gov.tr/openAcceptedDocument.htm?fileID=1418253&no=304688). Turk J Elec Eng & Comp Sci, ( DOI update soon) 


